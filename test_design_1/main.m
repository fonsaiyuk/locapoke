//
//  main.m
//  test_design_1
//
//  Created by Pham Duc Trung on 5/20/14.
//  Copyright (c) 2014 Pham Duc Trung. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
