//
//  ViewController.m
//  test_design_1
//
//  Created by Pham Duc Trung on 5/20/14.
//  Copyright (c) 2014 Pham Duc Trung. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

NSMutableArray *array1;
NSMutableArray *array2;
NSMutableArray *array3;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    array1 = [[NSMutableArray alloc] initWithObjects:@"1", @"2", @"3" , nil];
    array2 = [[NSMutableArray alloc] initWithObjects:@"4", @"5", @"6" , nil];
    array3 = [[NSMutableArray alloc] initWithObjects:@"7", @"8", @"9" , nil];
    
    myPicker.delegate = self;
    
}

-(NSInteger)numberOfComponentsInpickerView:(UIPickerView *)
    pickerView
{
    return 3;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component
{
    if(component == 0)
        return [array1 count];
    if(component == 1)
        return [array2 count];
    if(component == 2)
        return [array3 count];

    return 0;
}


-(void)pickerView:(UIPickerView *)pickerView
     didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
}

-(NSString *)pickerView:(UIPickerView *)pickerView
titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if(component == 0)
        return [array1 objectAtIndex:row];
    if(component == 1)
        return [array2 objectAtIndex:row];
    if(component == 2)
        return [array3 objectAtIndex:row];
    
    return @"";

}

//
//-(void)pickerView:(UIPickerView *)pickerView
//     didSelectRow:(NSInteger)row inComponent:(NSInteger)component
//
//{
//}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (IBAction)unwindFromView:(UIStoryboardSegue *)segue {}

@end
