//
//  AppDelegate.h
//  test_design_1
//
//  Created by Pham Duc Trung on 5/20/14.
//  Copyright (c) 2014 Pham Duc Trung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
